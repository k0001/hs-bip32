let pkgs = import ./nix;
in rec {
  ghc883 = pkgs._here.ghc883.bip32;
  ghc865 = pkgs._here.ghc865.bip32;
  ghcjs86 = pkgs._here.ghcjs86.bip32;
}
