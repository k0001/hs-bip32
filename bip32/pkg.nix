{ mkDerivation, base, base16-bytestring, base58-bytestring, binary
, bitcoin-hash, bitcoin-keys, bytestring, hedgehog, stdenv, tasty
, tasty-hedgehog, tasty-hunit, nix-gitignore
}:
mkDerivation {
  pname = "bip32";
  version = "0.2";
  src = nix-gitignore.gitignoreSourcePure ../.gitignore ./.;
  libraryHaskellDepends = [
    base base58-bytestring binary bitcoin-hash bitcoin-keys bytestring
  ];
  testHaskellDepends = [
    base base16-bytestring base58-bytestring binary bitcoin-keys
    bytestring hedgehog tasty tasty-hedgehog tasty-hunit
  ];
  homepage = "https://gitlab.com/k0001/hs-bip32";
  description = "BIP-0032: Hierarchical Deterministic Wallets for Bitcoin and other cryptocurrencies";
  license = stdenv.lib.licenses.asl20;
}
