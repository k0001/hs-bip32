# Version 0.2

* COMPILER ASSISTED BREAKING CHANGE — `Prv`, `prv`, `unPrv`, `prvToPub`, `Pub`,
  `pub` and `unPub` are not exported from the `BIP32` module anymore. Instead,
   this library now uses the corresponding types and functions from the
   `Bitcoin.Keys` module of the `bitcoin-keys` library.

* Add `indexNext`, `subsXPubXPub`, `subsPubPub`, `subsXPrvXPrv`, `subPrvPrv`,
  `subsXPrvXPub`, `subsPubPub`.

* Add field accessors to `XPub` and `XPrv`.

* Depend on `bitcoin-keys`.

* Improved `Show` instances.

* Improved documentation.


# Version 0.1.2

* Depend on `bitcoin-hash`.

* Export `unIndex`.


# Version 0.1.1

* Builds with GHCJS.


# Version 0.1

* Initial version.
